#!/bin/python
import selenium
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service

from bs4 import BeautifulSoup
import codecs
import re

import datetime

from IPython.display import display, HTML

import pymongo
from pymongo import MongoClient

client = MongoClient("localhost", 27017)

db = client.avalanchepi_test

forecasts = db.avalanche_forecasts.find()


# MONGO HELPER FUNCTIONS
def insert_avalanche_forecast(db, document):
    db.avalanche_forecasts.insert_one(document)

def generate_document(location_id, body, issue_date, expiration_date, authors, url):
    return { "location_id": location_id, "body": body, "issue_date": issue_date, "expiration_date": expiration_date, "authors": authors, "url": url, "accessed": datetime.datetime.now().timestamp()}

def set_all_avalance_forecasts_to_updated():
    # in the future this might only look at ids that are in this webscrapers avalanche zone
    db.avalanche_forecasts.update_many({"updated": { "$eq": "N"}}, {"$set": {"updated": "Y"}})
# TEXT PARSING HELPER FUNCTIONS
def get_authors(text):
    pattern = 'Author<.*>.*<.*>'
    authors = []
    for match in re.findall(pattern, str(text)):
        # loop through the string remove everything before the FIRST greater than sign and after the SECOND less than sign
        cut_start = -1
        cut_end = -1
        gt_count = 0
        lt_count = 0
        index = 0
        for c in match:
            if c == '>':
                gt_count += 1
                if gt_count == 1:
                    cut_start = index
            elif c == '<':
                lt_count += 1
                if lt_count == 2:
                    cut_end = index
            if gt_count == 1 and lt_count == 2:
                break
                # exit the loop and cut 
            index += 1
        authors.append(match[cut_start + 2:cut_end - 1]) # magic numbers remove extra characters around author name
    return authors 

def get_issue_date(text):
    return get_text('Issued<.*>.*<.*>', text)
def get_expiration_date(text):
    return get_text('Expires<.*>.*<.*>', text)

def get_text(pattern, text):
    # returns a list of items
    items = []

    for match in re.findall(pattern, str(text)):
        # loop through the string remove everything before the FIRST greater than sign and after the SECOND less than sign
        cut_start = -1
        cut_end = -1
        gt_count = 0
        lt_count = 0
        index = 0
        for c in match:
            if c == '>':
                gt_count += 1
                if gt_count == 1:
                    cut_start = index
            elif c == '<':
                lt_count += 1
                if lt_count == 2:
                    cut_end = index
            if gt_count == 1 and lt_count == 2:
                break
                # exit the loop and cut 
            index += 1
        items.append(match[cut_start + 2:cut_end - 1]) # magic numbers remove extra characters around author name
    return items

def get_body_text(body):
    matches = soup.body.find_all("p", "nac-html-p")
    result = ""
    for m in matches:


        result += str(m)
    return str(result)

def trim_unecessary_text(text):
    # After the end of the forecast there are article tags which have a lot of information that is NOT related to the avalanche forecast we are viewing
    # this must be trimmed out so that it is not confused with the forecast we are viewing
    return text[:text.index("<article")]

service = Service(executable_path=r'/usr/bin/chromedriver')
options=webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
driver = webdriver.Chrome(service=service, options=options)

snoqualmie_pass_url = "https://nwac.us/avalanche-forecast/#/snoqualmie-pass"

driver.get(snoqualmie_pass_url)

page_source=driver.page_source
soup = BeautifulSoup(page_source,features="html.parser")

#h_matches=soup.body.find_all("h6")
#div_matches=soup.body.find_all("div")
#insert_avalanche_forecast(db, generate_document(2, get_body_text(soup.body), "07/01/1989", "07/01/2023", "Author", snoqualmie_pass_url))




location_id = 80085 # TODO add records to the db so that this can be queried based off of the url
body_text = get_body_text(soup.body)
issue_date = get_issue_date(trim_unecessary_text(str(soup.body)))
expiration_date = get_expiration_date(trim_unecessary_text(str(soup.body)))
authors = get_authors(soup.body)






#projection = {"body:1"}
print(db.avalanche_forecasts.find({}, {"_id": 0, "body": 1}).sort([("accessed", pymongo.DESCENDING)]).limit(1)[0].get("body"))

exit()

insert_avalanche_forecast(db, generate_document(location_id, body_text, issue_date, expiration_date, authors, snoqualmie_pass_url))

exit()




str_body = str(soup.body)
#str_body = str_body[:str_body.index("article")]
for date in get_expiration_date(str_body):
    print(date)
exit()

#for date in get_issue_date(soup.body):
    #print(date)
#exit()
f = open("nwac_snoqualmie.txt", "w")

f.write(str(soup.body))

f.close()
exit()



count = 1
for i in div_matches:
    f.write(str(i) + "\n")
    count += 1
f.close()
driver.quit()

# TODO 
# update this to write to mongo db instead of file
# add all avalanche stuff
# get body, author, issued, expiration



