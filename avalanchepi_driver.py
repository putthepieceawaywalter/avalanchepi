#!/usr/bin/python

from flask import Flask, request, jsonify
import pymongo
from pymongo import MongoClient

client = MongoClient("localhost", 27017)
db = client.avalanchepi_test

app = Flask(__name__)

@app.route("/")
def home():
    return "home" 

@app.route("/get_forecast_by_id/<int:id>")
def get_forecast_by_id(id=-1):
    # id represents a unique id of an avalanche zone to be defined in a db later
    return "this is the forecast for id: " + str(id)

@app.route("/get_html_forecast_by_id/<int:id>")
def get_html_forecast_by_id(id=-1):
    return db.avalanche_forecasts.find({"location_id": id}, {"_id": 0, "body": 1}).sort([("accessed", pymongo.DESCENDING)]).limit(1)[0].get("body")


@app.route("/get_nearest_location_id/<string:location>")
def get_nearest_location_id(location=""):
    return "I'll find the nearest avalanche forecast to the location you provided, " + location

@app.route("/get_recent_avalanches_by_id/<int:id>")
def get_recent_avalanches_by_id(id=-1):
    return "these are the most recent avalanches"

@app.route("/get_recent_observations_by_id/<int:id>")
def get_recent_observations_by_id(id=-1):
    return "these are the most recent avalanche observations"

@app.route("/get_weather_forecast_by_id/<int:id>")
def get_weather_forecast_by_id(id=-1):
    return "this is the weather forecast for id " + str(id)

@app.route("/get_avalanche_zone_information_by_id/<int:id>")
def get_avalanche_zone_information_by_id(id=-1):
    return "this is all the info I have about " + str(id)







if __name__ == "__main__":
    app.run(debug=True)
